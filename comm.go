package comm

import (
	"gitlab.com/bomz/logger"
)

var log = logger.New()

// site root url
const RootUrl = "/"

// 세션 아이디 구분자 (예: 이메일@@@무작위이름)
const SessionIdDelimiter = "@@@"

const (
	/**	default error code	*/
	PANIC_CODE_DEFAULT_FAIL = 100
	/**	http template file parsing fail code 	*/
	PANIC_CODE_HTTP_TEMPLATE_PARSE = 110

	/**	encrypt fail	*/
	PANIC_CODE_ENCRYPT = 210
	/**	decrypt fail	*/
	PANIC_CODE_DECRYPT = 211

	/**	db connect fail code	*/
	PANIC_CODE_DB_CONN = 300
	/**	db transaction begin fail code	*/
	PANIC_CODE_DB_TRANSACTION = 301
	/**	db query fail	*/
	PANIC_CODE_DB_QUERY = 302
	/**	db session marshal or insert or connection fail	*/
	PANIC_CODE_DB_SESSION = 303

	/**	 사용자 지역정보 변경 결과 없음.	*/
	ERR_EMPTY_UPDATE_EMPLOYEE_UPDATE = "1100"
	/**	 로그인, 로그아웃 처리를 위해 쿠키정보 가져오기 오류	*/
	ERR_LOAD_FAIL_COOKIE = "1101"

	/**	파입업로드실패	*/
	ERR_FILE_UPLOAD_FAIL = "1200"
)
