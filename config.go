package comm

import (
	"io/ioutil"
	"strconv"
	"time"

	"gopkg.in/yaml.v2"
)

/**	Root Config struct	*/
type config struct {
	DbMain    dbConfig     `yaml:"dbMain"`
	DbSession dbConfig     `yaml:"dbSession"`
	System    systemConfig `yaml:"system"`
}

/**	Databse config struct	*/
type dbConfig struct {
	Ip              string `yaml:"ip"`              // db connection ip
	Port            int    `yaml:"port"`            // db connection port
	Name            string `yaml:"name"`            // db name
	Id              string `yaml:"id"`              // db user id
	Pw              string `yaml:"pw"`              // db user password
	ConnMaxLifetime int64  `yaml:"connMaxLifetime"` // db max connection life time (minute)
	MaxOpenConns    int    `yaml:"maxOpenConns"`    // db max connection length
	MaxIdleConns    int    `yaml:"maxIdleConns"`    // db idle connection length
}

/**	System config struct	*/
type systemConfig struct {
	SessionMaximum int    `yaml:"sessionMaximum"` // 사용자 동시 로그인 최대 수
	SessionTimeout int    `yaml:"sessionTimeout"` // 사용자 자동 로그아웃 시간 (단위:분)
	FileUploadPath string `yaml:"fileUploadPath"` // 파일 업로드 기본 경로
}

var configInfo config

/**	설정파일 읽은 후 초기화	*/
func InitConfig() {
	d, e := ioutil.ReadFile("./bz.yaml")
	if e != nil {
		log.Err("bz.yaml read fail. ", e.Error())
		panic("bz.yaml read fail. " + e.Error())
	}

	c := config{}
	er := yaml.Unmarshal(d, &c)
	if er != nil {
		log.Err("bz.yaml marshal error. ", er.Error())
		panic("bz.yaml marshal error. " + er.Error())
	}

	configInfo = c

	if log.IsInfo() {
		log.Info("config init finish")
	}

	if log.IsDebug() {
		log.Debug("config infos", configInfo)
	}
}

/**	메인 디비 접속 정보 리턴	*/
func GetMainDatabaseConnectInfo() string {
	// 리턴 문자열 예) 아이디:암호@tcp(아이피:포트)/디비이름"
	return configInfo.DbMain.Id + ":" +
		configInfo.DbMain.Pw + "@tcp(" +
		configInfo.DbMain.Ip + ":" +
		strconv.Itoa(configInfo.DbMain.Port) + ")/" +
		configInfo.DbMain.Name
}

/**	메인 디비 컨넥션 생명유지 시간 리턴 (단위:분)	*/
func GetConnMaxLifetime() int64 {
	return configInfo.DbMain.ConnMaxLifetime
}

/**	메인 디비 컨넥션 최대 유지수 리턴	*/
func GetMaxOpenConns() int {
	return configInfo.DbMain.MaxOpenConns
}

/**	메인 디비풀 컨넥션 최대 유지수 리턴	*/
func GetMaxIdleConns() int {
	return configInfo.DbMain.MaxIdleConns
}

/**	세션 디비 접속 주소	*/
func GetSessionDatabaseAddr() string {
	return configInfo.DbSession.Ip + ":" +
		strconv.Itoa(configInfo.DbSession.Port)
}

/**	세션 디비 접속 비밀번호	*/
func GetSessionDatabasePassword() string {
	return configInfo.DbSession.Pw
}

/**	세션 동시 접속 최대 수	*/
func GetSessionMaximum() int{
		return configInfo.System.SessionMaximum
}

/**	세션 자동 로그아웃 시간	*/
func GetSessionTimeout() time.Duration {
	if configInfo.System.SessionTimeout <= 0 {
		configInfo.System.SessionTimeout = 1 // 최소 1시간으로 설정
	}

	return time.Duration(configInfo.System.SessionTimeout) * time.Minute
}

/**	파일 업로드 기본 경로	*/
func GetFileUploadPath() string {
	return configInfo.System.FileUploadPath
}
