package comm

import (
	"encoding/json"
)

//핸들러 처리 중 패닉 발생 시 응답 구조체
type PanicRes struct {
	Msg  interface{} // panic message
	Code int         // panic code
}

// 핸들러 에러 내용을 json 현식으로 리턴
func (r *PanicRes) ToJSON() *[]byte {
	w, err := json.MarshalIndent(&r, "", "\t\t")

	if err != nil {
		return &[]byte{}
	} else {
		return &w
	}
}
