package comm

import (
	"encoding/json"
	"errors"
	"reflect"
)

/**	HTML Result Type	*/
var TypeIsHtmlResult = reflect.TypeOf(&HtmlResult{})

/**	Json Result Type	*/
var TypeIsJsonResult = reflect.TypeOf(&JsonResult{})

/**	Redirect Result Type	*/
var TypeIsRedirectResult = reflect.TypeOf(&RedirectResult{})

/**	File Result Type	*/
var TypeIsFileResult = reflect.TypeOf(&FileResult{})

type csrf struct {
	CsrfCode string
}

type Result interface {
	GetValue() *[]byte // response data
	GetError() *[]byte // response error
}

/**	FILE Result	*/
type FileResult struct {
	file string
	fail *JsonResult
}

/**	FILE Result Make	*/
func NewFileResult(f string) *FileResult {
	return &FileResult{file: f}
}

func (f *FileResult) GetValue() *[]byte {
	v := []byte(f.file)
	return &v
}

func (f *FileResult) GetError() *[]byte {
	return nil
}

/**	HTML Template Result	*/
type HtmlResult struct {
	htmlFile []byte
}

/**	HTML Template Make	*/
func NewHtmlFile(hf string) *HtmlResult {
	return &HtmlResult{[]byte(hf)}
}

/**	유효성 검사 등의 실패 응답일 경우	*/
func NewFailFileResult(e error, p1 string, p2 string) *FileResult {

	if e == nil {
		return &FileResult{fail: &JsonResult{data: new([]byte)}} // 만약을 위한 처리
	}

	if log.IsDebug() {
		log.Debug("file response fail.", e, " / param1=", p1, " / param2=", p2)
	}

	e1 := ErrorResult{ErrMsg: e.Error(), ErrParam1: p1, ErrParam2: p2}
	return &FileResult{fail: &JsonResult{new([]byte), &e1}}
}

func (h *HtmlResult) GetValue() *[]byte {
	return &h.htmlFile
}

func (h *HtmlResult) GetError() *[]byte {
	return nil
}

/**	JSON Error Result	*/
type ErrorResult struct {
	ErrMsg    string
	ErrParam1 string
	ErrParam2 string
}

/**	JSON Result	*/
type JsonResult struct {
	data *[]byte
	er   *ErrorResult
}

/**	Json Result Make	*/
func NewJsonResult(v interface{}) *JsonResult {
	if v == nil {
		return &JsonResult{data: new([]byte)}
	}

	if output, e := json.MarshalIndent(&v, "", "\t\t"); e != nil {
		// marshal fail
		e1 := ErrorResult{ErrMsg: e.Error()}
		return &JsonResult{data: new([]byte), er: &e1}
	} else {
		// marshal ok
		return &JsonResult{data: &output}
	}

}

/**	처리 중 정확한 내용을 클라이언트에 표시할 수 없고 에러코드 값만 표시할 경우	*/
func NewFailJsonResultInErrorCode(cd string) *JsonResult {
	return NewFailJsonResult(errors.New("errMsg"), cd, "")
}

/**	유효성 검사 등의 실패 응답일 경우	*/
func NewFailJsonResult(e error, p1 string, p2 string) *JsonResult {

	if e == nil {
		return &JsonResult{data: new([]byte)} // 만약을 위한 처리
	}

	if log.IsDebug() {
		log.Debug("json response fail.", e, " / param1=", p1, " / param2=", p2)
	}

	e1 := ErrorResult{ErrMsg: e.Error(), ErrParam1: p1, ErrParam2: p2}
	return &JsonResult{new([]byte), &e1}
}

func (r *JsonResult) GetValue() *[]byte {
	if r.er != nil {
		return nil
	}

	return r.data
}

func (r *JsonResult) GetError() *[]byte {
	if r.er != nil {
		if w, err := json.MarshalIndent(&r.er, "", "\t\t"); err != nil {
			return &[]byte{}
		} else {
			return &w
		}
	}

	return nil
}

/**	Redirect Result		*/
type RedirectResult struct {
	url []byte
}

/**	Redirect Make	*/
func NewRedirect(url string) *RedirectResult {
	return &RedirectResult{[]byte(url)}
}

func (h *RedirectResult) GetValue() *[]byte {
	return &h.url
}

func (h *RedirectResult) GetError() *[]byte {
	return nil
}
