package comm

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha512"
	"encoding/base64"
	"entity"
	"errors"
	"fmt"
	"io"
	mr "math/rand"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

/**	무작위 이름 생성		*/
func MakeRandomName() string {
	var sb strings.Builder
	sb.WriteString(strconv.Itoa(time.Now().Nanosecond()))
	for {
		sb.WriteString(strconv.Itoa(mr.Intn(20000)))
		if sb.Len() >= 16 {
			break
		}
	}

	if sb.Len() == 16 {
		return sb.String()
	}

	return sb.String()[0:16]
}

/**	SHA512 기법 암호화 처리	*/
func EncryptSha512(v *string) (string, error) {
	h := sha512.New()
	if _, e := io.WriteString(h, *v); e != nil {
		return "", e
	}

	return fmt.Sprintf("%x", h.Sum(nil)), nil
}

/**	AES 기법 암호화 처리	*/
func EncryptAes(key []byte, org string) (string, error) {
	if len(key) != aes.BlockSize {
		return "", errors.New("AES Key length fail. [len=" + strconv.Itoa(len(key)) + "]")
	}

	plainText := []byte(org)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	cipherText := make([]byte, aes.BlockSize+len(plainText))
	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(cipherText[aes.BlockSize:], plainText)

	return base64.URLEncoding.EncodeToString(cipherText), nil
}

/**	AES 기법 복호화 처리	*/
func DecryptAes(key []byte, msg string) (string, error) {
	if len(key) != aes.BlockSize {
		return "", errors.New("AES Key length fail. [len=" + strconv.Itoa(len(key)) + "]")
	}

	cipherText, err := base64.URLEncoding.DecodeString(msg)
	if err != nil {
		return "", err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(cipherText, cipherText)

	return string(cipherText), nil
}

/**	문자열 패턴 검사. 숫자+영어+한글+공백+일부 특수문자 허용		*/
func PatternAll(v *string) bool {
	return patternCheck("[^0-9a-zA-Zㄱ-ㅎ가-힣ㅏ-ㅣ~!@#$().,'\"?\\[\\]\\s_]", v)
}

/**	문자열 패턴 검사. 숫자만 허용		*/
func PatternNumber(v *string) bool {
	return patternCheck("[^0-9]", v)
}

/** 문자열 패턴 검사. 알파벳만 허용	*/
func PatternAlphabat(v *string) bool {
	return patternCheck("[^a-zA-Z]", v)
}

/** 이메일 패턴 검사		*/
func PatternEmail(v *string) bool {
	return !patternCheck("^[0-9a-zA-Z]([-_.']?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$", v)
}

func patternCheck(pt string, v *string) bool {
	if v == nil {
		return false
	}

	r, err := regexp.MatchString(pt, *v)
	if err != nil {
		return false
	}

	return !r
}

// 글자 수 및 패턴 유효성 검사
func ValidationText(v *string, min int, max int) *JsonResult {
	if v == nil {
		return NewFailJsonResult(errors.New("emptyParam"), "", "")
	}

	*v = strings.Trim(*v, " ")

	len := utf8.RuneCountInString(*v)
	if len < min || len > max {
		return NewFailJsonResult(errors.New("failValueLength"), strconv.Itoa(min), strconv.Itoa(max))
	}

	if !PatternAll(v) {
		return NewFailJsonResult(errors.New("failPatternAll"), "", "")
	}

	return nil
}

// Idxs 목록 유효성 검사 (최소 1개 이상이여야 하고 모든 값은 1 이상이어야 한다)
func ValidationIdxs(v *entity.Idxs) *JsonResult {
	if v == nil || v.Idxs == nil || len(v.Idxs) == 0 {
		return NewFailJsonResult(errors.New("emptySelectItem"), "", "")
	}

	if len(v.Idxs) > 200 {
		return NewFailJsonResult(errors.New("failMaxReq"), "200", "")
	}

	return nil
}
